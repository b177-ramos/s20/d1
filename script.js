// JSON Object
// -- JavaScript Object Notation
// -- Common use is to read data from webserver, and display the data ng a web page.
/* Syntax:
	{
	"propertyA": "valueA",
	"propertyB": "valueB"
	}*/




// JSON Objects
// {
// 	"city": "Quezon City",
// 	"province": "Metro Manila",
// 	"country": "Philippines"
// }




// [SECTION] JSON Array
// "cities":[
// 	{"city": "Quezon City", "province": "Metro Manila", "country": "Philippines"}
// 	{"city": "Pasig City", "province": "Metro Manila", "country": "Philippines"}
// 	{"city": "Navotas City", "province": "Metro Manila", "country": "Philippines"}
// ]




// [SECTION] JSON Methods
// JSON.stringify -- converts JavaScript Object/Array into string
let batchesArray = [
	{batchName: 'Batch177'},
	{batchName: 'Batch178'},
	{batchName: 'Batch179'}
];

console.log(batchesArray);
let batchesArrayCont = JSON.stringify(batchesArray);
console.log(`Result from stringify method`);
console.log(batchesArrayCont);

let data = JSON.stringify({
	name: `John`,
	age: 31,
	address: {
		city: `Manila`,
		country: `Philippines`
	}
});


let stringifiedObject = `{
	"name":"John",
	"age":31,
	"address":{
		"city":"Manila",
		"country":"Philippines"
	}
}`
console.log(data);	

console.log(JSON.parse(stringifiedObject));



// JSON.parse
console.log(`Result from parse method: `);
let dataParse = JSON.parse(data)
console.log(dataParse);
console.log(dataParse.name);

console.log(JSON.parse(batchesArrayCont));